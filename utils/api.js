import Mock from './mock.js'
let API_HOST = "http://129.211.169.131:13688"; // 这里是后端接口地址，要换成真正的地址
let DEBUG = true; //切换数据入口，如果后端接口部署完毕要换成false，使用mock数据就改为true

const testData = {
	"getCookList": [
		{ "id": 0, "date": "05-09 12：40 周三 名厨上门", "address": " 洪山区烽胜路烽火崇文兰庭小区57号商铺", "name": "张小斐", "tel": "89575155", "img": "../static/image/goods4.png", "goodsname": "麻辣小龙虾", "price": 188 },
		{ "id": 1, "date": "05-10 13：40 周四 我来上门", "address": " 湖北省武汉市硚口区长丰大道166号", "name": "贾玲", "tel": "88100666", "img": "../static/image/goods1.jpg", "goodsname": "和牛拉面", "price": 128 },
		{ "id": 2, "date": "05-11 14：40 周五 名厨上门", "address": " 湖北省武汉市青山区工业一路33-1号附近", "name": "沈腾", "tel": "89575111", "img": "../static/image/goods3.jpg", "goodsname": "避风塘炒蟹", "price": 288 },
		{ "id": 3, "date": "05-12 15：40 周六 你来上门", "address": " 湖北省武汉市新洲区博物大道东城尚品东南角", "name": "李连杰", "tel": "88100999", "img": "../static/image/goods2.jpg", "goodsname": "法式罗宋汤", "price": 88 }
	],
	"getCarList": [
		{ "id": 1,"name": "商品1", "img": "../static/images/3.png", "num": 0, "price": 88 },
		{ "id": 2,"name": "商品2", "img": "../static/images/4.png", "num": 0, "price": 99 },
		{ "id": 3,"name": "商品3", "img": "../static/images/5.png", "num": 0, "price": 77 }
	],
	"getGoodsList": [
		{ "id": 0, "img": "../static/image/goods4.png", "goodsname": "麻辣小龙虾", "price": 188 },
		{ "id": 1, "img": "../static/image/goods1.jpg", "goodsname": "和牛拉面", "price": 128 },
		{ "id": 2, "img": "../static/image/goods3.jpg", "goodsname": "避风塘炒蟹", "price": 188 },
		{ "id": 3, "img": "../static/image/goods2.jpg", "goodsname": "法式罗宋汤", "price": 88 }
	],
	"getManList": [
		{ "id": 1, "img": "../static/images/man1.jpg", "name": "蔡亦凡", "rating": 4.5 },
		{ "id": 2, "img": "../static/images/man2.jpg", "name": "吴有林", "rating": 5 },
		{ "id": 3, "img": "../static/images/man3.jpg", "name": "李大龙", "rating": 3.5 }
	],
	"getNavList": [
		{"id": 1, "img": "../static/images/7.png", "text": "名厨"},
		{"id": 2, "img": "../static/images/5.png", "text": "餐宴"},
		{"id": 3, "img": "../static/images/4.png", "text": "坝坝宴"},
		{"id": 4, "img": "../static/images/6.png", "text": "周边"}
	]
}

/**
 *
 * @param {string} url 请求地址
 * @param {Object} data 请求参数
 * @param {string} method 请求方法
 * @param {Object} header 自定义请求头
 * @returns Promise
 */
function ajax(url = '', data = {}, method = "get", header = { "Content-Type": "application/json" }) {
    if (!DEBUG) {
		return new Promise((resolve, reject) => {
			// 这里是发送真正的请求，请求后端接口
			wx.request({
				url: API_HOST + url,
				method,
				data,
				header,
				success: function (res) {
					resolve(res.data)
				}
			});
		})
    } else {
        return new Promise((resolve, reject) => {
          // 返回mock随机数据
   //        let mockOptions = {
   //          status: 1,
   //          msg: 'success',
			// 'data|1-10': [{
			// 	id: '@increment()', // 自增长id
			// 	title: '@ctitle(1, 50)', // 长度1-50的中文标题
			// 	'category|+1': ['javascript', 'html', 'css', 'vue'], // 四选一
			// 	content: '@cparagraph(1, 50)', // 长度1-50的段落文本
			// 	author: '管理员', // 作者
			// 	'images|3': [ // 随机图片
			// 		'@image( "94x94", #CCC, #000, "png", "picture" )'
			// 	],
			// 	star: '@natural(1, 5)', // 1-5的数字
			// 	'comments|1-5': [ // 1-5条评论
			// 		{
			// 			avator: '这是头像图片',
			// 			content: '这是是评论内容',
			// 			author: '评论者',
			// 			thumbNum: '@natural(0, 50)', // 0-50的整数
			// 			created: '@datetime()' // 时间字符串
			// 		}
			// 	],
			// 	readedNum: '@natural(0, 50)', // 0-50的整数
			// 	sharedNum: '@natural(0, 50)', // 0-50的整数
			// 	favoritesNum: '@natural(0, 50)', // 0-50的整数
			// 	thumbNum: '@natural(0, 50)', // 0-50的整数
			// 	created: '@datetime()', // 时间字符串
			// 	updated: '@datetime()', // 时间字符串
			// }]
   //        }

          resolve(Mock.mock(testData[url.split('/').slice(-1)[0]])) // 使用随机的mock数据要换成 resolve(Mock.mock(mockOptions))
        })
    }
}
module.exports = {
    ajax
}