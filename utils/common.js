/**
 *
 * @param {timeStamp} datetime 时间戳或时间字符串
 * @param {string} pattern 格式
 * @param {boolean} addon 是否显示周
 */
export function dateFormat(
  datetime = Date.now(),
  pattern = 'yyyy/MM/dd hh:mm:ss',
  addon = false
) {
  let rPattern = pattern
  const vWeek = [
    '星期天',
    '星期一',
    '星期二',
    '星期三',
    '星期四',
    '星期五',
    '星期六'
  ]
  const dt = new Date(datetime)
  const y = dt.getFullYear()
  const m = (dt.getMonth() + 1).toString().padStart(2, '0')
  const d = dt
    .getDate()
    .toString()
    .padStart(2, '0')
  const hh = dt
    .getHours()
    .toString()
    .padStart(2, '0')
  const mm = dt
    .getMinutes()
    .toString()
    .padStart(2, '0')
  const ss = dt
    .getSeconds()
    .toString()
    .padStart(2, '0')
  const vDWeek = dt.getDay() // 星期

  rPattern = rPattern.replace(/y{4}/, `${y}`)
  rPattern = rPattern.replace(/(\W)?M{2}/, `$1${m}`)
  rPattern = rPattern.replace(/(\W)?d{2}/, `$1${d}`)
  rPattern = rPattern.replace(/(\s)?h{2}/, `$1${hh}`)
  rPattern = rPattern.replace(/(\W)?m{2}/, `$1${mm}`)
  rPattern = rPattern.replace(/(\W)?s{2}/, `$1${ss}`)
  rPattern += addon ? ` ${vWeek[vDWeek]}` : ''
  return rPattern
}